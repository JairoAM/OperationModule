package com.example.OperationModule.controllers;

import com.example.OperationModule.model.CalculateDTO;
import com.example.OperationModule.service.CalculateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api")
public class CalculateController {
    @Autowired
    CalculateService calculateService;

    /**
     * Method to calculate the value of K, to check if it meets the condition 0 ≤ K ≤ N with K mod X = Y.
     * @param x number x
     * @param y number y
     * @param n number n
     * @return an object with the input parameters (X, Y, and N) and the value of K
     */
    @GetMapping("/calculateModule")
    public ResponseEntity<CalculateDTO> getCalculate (@RequestParam("x") int x, @RequestParam("y")  int y,@RequestParam("n")  int n) {
        int k = calculateService.calculateModule(x, y, n);
        CalculateDTO calculate = new CalculateDTO(x, y, n, k);
        // The condition "0 ≤ K ≤ N" is checked.
        if (k >= n || k <= 0){
            calculate.setCode("1");
            calculate.setTitle("Error en el cálculo");
            calculate.setDescription("No se encuentra un número entero positivo mayor o igual que 0 y menor o igual que N");
            return new ResponseEntity<>(calculate, HttpStatus.BAD_REQUEST);
        } else {
            calculate.setCode("0");
            calculate.setTitle("Exito");
            calculate.setDescription("Ok");
            return new ResponseEntity<>(calculate, HttpStatus.OK);
        }
    }

    /**
     * Method to calculate the value K, to check if it meets the condition 0 ≤ K ≤ N.
     * @param calculate object with the input parameters X, Y, and N
     * @return an object with the input parameters (X, Y, and N) and the value of K
     */
    @PostMapping ("/calculateModule")
    public ResponseEntity<CalculateDTO> postCalculate(@RequestBody CalculateDTO calculate) {
        int k = calculateService.calculateModule(calculate.getX(), calculate.getY(), calculate.getN());
        calculate.setK(k);
        // The condition "0 ≤ K ≤ N" is checked.
        if (k >= calculate.getN() || k <= 0){
            calculate.setCode("1");
            calculate.setTitle("Error en el cálculo");
            calculate.setDescription("No se encuentra un número entero positivo mayor o igual que 0 y menor o igual que N");
            return new ResponseEntity<>(calculate, HttpStatus.BAD_REQUEST);
        } else {
            calculate.setCode("0");
            calculate.setTitle("Exito");
            calculate.setDescription("Ok");
            return new ResponseEntity<>(calculate, HttpStatus.OK);
        }
    }
}