package com.example.OperationModule.service;

import org.springframework.stereotype.Service;

@Service
public class CalculateServiceImpl implements CalculateService {

    /**
     *
     * @param x number x
     * @param y number y
     * @param n number n
     * @return the value of K for which K mod X = Y
     */
    @Override
    public int calculateModule(int x, int y, int n) {

        return n - ((n - y) % x);
    }
}
