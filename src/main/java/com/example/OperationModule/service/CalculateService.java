package com.example.OperationModule.service;

public interface CalculateService {
    /**
     *
     * @param x number x
     * @param y number y
     * @param n number n
     * @return the value of K for which K mod X = Y
     */
    int calculateModule(int x, int y, int n);
}
