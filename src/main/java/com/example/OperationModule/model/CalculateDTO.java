package com.example.OperationModule.model;

public class CalculateDTO extends ErrorDto{
    int x;
    int y;
    int n;
    int k;

    public CalculateDTO(int x, int y, int n, int k) {
        this.x = x;
        this.y = y;
        this.n = n;
        this.k = k;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public int getK() {
        return k;
    }

    public void setK(int k) {
        this.k = k;
    }
}
