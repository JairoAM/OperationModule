package com.example.OperationModule;

import com.example.OperationModule.controllers.CalculateController;
import com.example.OperationModule.model.CalculateDTO;
import com.example.OperationModule.service.CalculateService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
public class CalculateControllerTest {

    @Mock
    private CalculateService calculateService;

    @InjectMocks
    private CalculateController calculateController;

    @Test
    public void testGetCalculate_Success() {
        int x = 7;
        int y = 5;
        int n = 12345;
        int expectedK = 12339;

        when(calculateService.calculateModule(x, y, n)).thenReturn(expectedK);

        ResponseEntity<CalculateDTO> response = calculateController.getCalculate(x, y, n);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedK, response.getBody().getK());
    }

    @Test
    public void testGetCalculate_Error() {
        int x = 17;
        int y = 8;
        int n = 1;

        when(calculateService.calculateModule(x, y, n)).thenReturn(0);

        ResponseEntity<CalculateDTO> response = calculateController.getCalculate(x, y, n);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testPostCalculate_Success() {
        CalculateDTO calculateDTO = new CalculateDTO(7, 5, 12345, 12339);

        when(calculateService.calculateModule(7, 5, 12345)).thenReturn(12339);

        ResponseEntity<CalculateDTO> response = calculateController.postCalculate(calculateDTO);

        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testPostCalculate_Error() {
        CalculateDTO calculateDTO = new CalculateDTO(17, 8, 1, 0);

        when(calculateService.calculateModule(17, 8, 1)).thenReturn(0);

        ResponseEntity<CalculateDTO> response = calculateController.postCalculate(calculateDTO);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }
}
